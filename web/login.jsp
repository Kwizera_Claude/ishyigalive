<%-- 
    Document   : login
    Created on : Feb 19, 2022, 8:48:41 PM
    Author     : Kwizera Claude
--%>
<%@page import="entities.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ishyiga Live</title>

    </head>
    <body>
        <%@include file="heading.jsp" %>
        <div class="container text-center"><div class="text-center">
                <% String message="";     
           message=((String)request.getAttribute("message"));            
        %>
        <label class="alert-danger"><%if(message!=null)%><%=message%></label>
            </div>
                    <div class="row"><div class="breadcrumb"><div class="breadcrumb-item"><a href="<%=request.getContextPath()%>/">Home</a></div>  
                        <div class="breadcrumb-item"><a href="signup.jsp">Signup</a></div> 
                        <div class="breadcrumb-item active">Login</div></div>
                    <div class="col-12"><h3>Login-page</h3><hr/></div> 
                    
                </div>
                <div class="row"><div class="col-12 col-md-5">
                        <div class="m-1 pl-5 pr-5">
                            <form action="login" method="post">
                                <div class="form-group">
                                    <label for="lphone">Phone number</label>
                                    <input type="tel" id="phone" name="lphone" placeholder="07...." minlength="10" maxlength="10" pattern="^\d{10}$" required=""/>
                                  
                                </div>
                                  <div class="form-group">
                                    <label for="lpassword">Your password</label>
                                    <input type="password"  id="password" name="lpassword" required=""/>
                                </div>
                                <div class="form-group checkbox">
                                    <label class="checkbox">
                                        <input type="checkbox" name="remember"/>
                                        Remember me
                                    </label>
                                </div>
                                <input class="form-control bg-success btn-lg w-50 offset-5" type="submit" value="login">
                            </form>
                        </div>
                    </div>
                
              
                
            </div>
    </body>
</html>
