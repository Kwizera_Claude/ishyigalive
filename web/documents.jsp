<%-- 
    Document   : documents
    Created on : Feb 26, 2022, 1:55:20 PM
    Author     : Kwizera Claude
--%>

<%@page import="java.util.List"%>
<%@page import="controllers.DocumentController"%>
<%@page import="entities.User"%>
<%@page import="java.util.ArrayList"%>
<%@page import="entities.Document"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    
    <body>
            <%@include file="heading.jsp" %>
            <div class="row"><div class="breadcrumb"><div class="breadcrumb-item"><a href="<%=request.getContextPath()%>/login">Login</a></div>  
                        <div class="breadcrumb-item"><a href="signup.jsp">Signup</a></div> 
                          <div class="breadcrumb-item"><a href="<%=request.getContextPath()%>/">Home</a></div> 
                        <div class="breadcrumb-item active">Documents</div></div>
                 
                    
                </div>
        <%
          User user=(User)(request.getSession().getAttribute("user"));
         if(user==null)
          response.sendRedirect("login.jsp");
         else{  

         %>
        <div class="row">
         <div class=" card w-30 badge-light col-12 col-md-5 align-items-start ">
             <div class="card-body align-items-center">
                  <button type="button" class="btn btn-primary px-3">
                      <i class="fas fa-user-check" style="font-size:20px;"></i> 
                      <label>Names: <%=user.getFname()+" "%></label> 
                 <label><%=user.getLname()%></label></button>
                <br>
                 <label>Email: <%=user.getEmail()%></label><br>
                 <label>Phone Number: <%=user.getPhoneNum()%></label>
             </div>
         </div>
                      <div class=" card w-30 badge-light col-12 col-md-5 align-items-start ">
             <div class="card-body align-items-center">
               <form action="upload" method="post"
                        enctype="multipart/form-data">
                   <input type="file" name="file" size="50" />
                    <br />
                   <input type="submit" value="Upload" />
                  </form>
             </div>
         </div>
        </div>
        <div class="container col-12 align-items-start ml-md-3">
            <div class="row"><h4>Your documents:</h4></div>
           
               <% //System.out.print("User>>>>"+user.toString());
                DocumentController docController=new DocumentController();
              
                    long userId=user.getId();
                    List<Document> documents=docController.findDocuments(user); 
                     if(documents!=null){ int i=1;%>
            <% for(Document document:documents){ %>
            <div class="row align-items-center m-3">
                <form method="get" action="download">
                    <label><%=i%>. </label> <%=document.getName()%><a href="download"><button  name="url" value="<%=document.getUrl()%>" class="btn-link ml-3">Download</button></a>
                </form>
            </div>
            <% i++;} }}%>
         
        </div>
            
    </body>
</html>
