<%-- 
    Document   : fill_form
    Created on : Feb 19, 2022, 9:11:00 AM
    Author     : Kwizera Claude
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ishyiga Live</title>
<!--        <link rel="stylesheet" href="resource/bootstrap-4.0.0/dist/css/bootstrap.min.css"/>-->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
      
      
    </head>
    <body>
         <header>
             <nav class="navbar navbar-expand-md navbar-dark" style="background-color:#6666ff">
                    <div>
                        <a href="https://www.ishyiga.net" class="navbar-brand"> Ishyiga live</a>
                    </div>
                    <ul class="navbar-nav">
                        <li><a href="<%=request.getContextPath()%>/documents" class="nav-link">Documents</a></li>
                    </ul> 
                
                </nav>
            </header>
            <br>
       
        <div class="container">
           
            <h6 class="jumbotron col-sm-12 col-md">Hello Stranger! this is Ishyiga live where you fill all the required information accordingly</h6>
       <div class="breadcrumb text-capitalize bg-light justify-content-center">fill the form</div>
        <form class="form-group col-lg-6 col-sm-12 col-md justify-content-center" action="register" method="post">
       
            First name <input class="form-control"  type="text" name="fname" id="fname" required=""/><br>
            Last name <input class="form-control"  type="text" name="lname" id="lname"/><br>
            <label for="gender">Gender:</label>
            Male  <input type="radio" name="gender" value="male"/> Female  <input value="female" type="radio" name="gender"/> <br>
            <label for="phone">Enter your phone number</label><br>
            <input type="tel" id="phone" name="phone" placeholder="07..."  minlength="10" maxlength="10" pattern="^\d{10}$"><br>
            Email   <input class="form-control"  type="email" name="email" id="email" required="please fill the email "/><br>
            Password  <input class="form-control"  type="password" name="password" id="password"/><br>
            <input class="form-control bg-success btn-lg"  type="submit" name="submit" value="Register"/>
        </form>
       click here to login <a href="login.jsp">login</a>
        </div>
    </body>
</html>
