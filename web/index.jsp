<%-- 
    Document   : index
    Created on : Feb 19, 2022, 9:43:36 AM
    Author     : Kwizera Claude
--%>

<%@page import="controllers.DocumentController"%>
<%@page import="entities.VsdcData"%>
<%@page import="entities.Document"%>
<%@page import="java.util.ArrayList"%>
<%@page import="entities.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ishyiga Live</title>
         <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
      
           <style>
               .custom-control-input{
                 
               margin-top : 14px !important;

              margin-right:10px !important;

                 }
               table{
                   width:100%; border-collapse: collapse;
                  
               }
               tr{
                   height: 10px;
               }
               td{
                   border:1px solid #000;
                  font-size: 15px;
                  font-style: oblique;
               }
               td.data{
                   width:5%;
               }
               input{
                   width: 100%; height: 100%;
               }
              input.checkbox{
                   width: 20px; height: 20px;
               }
               @media print {
        body * {
    visibility: hidden;
  }
  #section-to-print, #section-to-print * {
    visibility: visible;
  }
  #section-to-print {
    position: absolute;
    left: 0;
    top: 0;
  }
}
           </style>
           
    </head>
    <body>
       
        <%@include file="heading.jsp" %>
        <div class="row"><div class="breadcrumb"><div class="breadcrumb-item"><a href="<%=request.getContextPath()%>/login">Login</a></div>  
                        <div class="breadcrumb-item"><a href="signup.jsp">Signup</a></div> 
                          <div class="breadcrumb-item"><a href="<%=request.getContextPath()%>/documents">Documents</a></div> 
                        <div class="breadcrumb-item active">Home</div></div>
                 
                    
                </div>
        <%
          User user=(User)(request.getSession().getAttribute("user"));
         if(user==null)
          response.sendRedirect("login.jsp");
         else{  

         %>
        <div class="row">
         <div class=" card w-30 badge-light col-12 col-md-5 align-items-start ">
             <div class="card-body align-items-center">
                  <button type="button" class="btn btn-primary px-3">
                      <i class="fas fa-user-check" style="font-size:20px;"></i> 
                      <label>Names: <%=user.getFname()+" "%></label> 
                 <label><%=user.getLname()%></label></button>
                <br>
                 <label>Email: <%=user.getEmail()%></label><br>
                 <label>Phone Number: <%=user.getPhoneNum()%></label>
             </div>
         </div>
                      <div class=" card w-30 badge-light col-12 col-md-5 align-items-start ">
             <div class="card-body align-items-center">
               <form action="upload" method="post"
                        enctype="multipart/form-data">
                   <input type="file" name="file" size="50" />
                    <br />
                   <input type="submit" value="Upload" />
                  </form>
             </div>
         </div>
        </div>
                 <%}%>
                 <div class="container col-8 col-sm-10 mt-2 mb-3">
                     <div class="row align-items-center align-content-center ml-md-5">
                         <% VsdcData vsdc=(VsdcData)request.getSession().getAttribute("vsdc");
                         %>                
                         <%
                         if(vsdc!=null){
                       
                               %>
                               <% if(vsdc.getStatus()==0){%>
                               <div class="alert-warning align-items-center m-2"><label>Pending documment...</label></div>
                               <%} else{%>
                                <div class="alert-success align-items-center m-2"><label>Document is read!!</label></div>
                                <%}%>
                                <form action="update" method="post">
                                <div id="section-to-print" class="section-to-print">
                     <div class="row">
                         <table class="">
                             
                             <tr class="">
                                 <td rowspan="2">
                                     <img src="#" alt="RRA"/>
                                 </td>
                                     <td class="text-capitalize text-uppercase">
                                     RWANDA REVENUE AUTHORITY
                                    </td>
                                     </tr>
                                   <tr class="">
                                 <td class="text-capitalize text-uppercase">
                                     Confirmation of Reception of VSDC SOFTWARE
                                 </td>
                          
                             </tr>
                         </table>
                     </div>
                     <div class="row mt-2">
                        
                         <table>
                             <tr>
                                 <td class="text-capitalize active bg-light" colspan="10">1.Company Information</td>
                        
                                 <tr>
                                 <td>Company Name</td>
                                 <td colspan="9"><input type="text" name="company" value="<%=vsdc.getCompany()%>"</td>
                             </tr>
                             <tr>
                                 <td>TIN</td>
                                 <td colspan="9"><input type="number" name="tin"  minlength="9" maxlength="9" value="<%=vsdc.getTin()%>"</td>
                             </tr>
                                  </tr>
                                <tr>
                                 <td>Address</td>
                                 <td colspan="9"><input type="text" name="c_address" value="<%=vsdc.getC_address()%>"</td>
                             </tr>
                              <tr>
                                 <td>Email</td>
                                 <td colspan="9"><input type="text" name="c_email" value="<%=vsdc.getC_email()%>"</td>
                             </tr>
                              <tr>
                                 <td>Phone</td>
                                 <td colspan="9"><input type="text" name="c_phone" value="<%=vsdc.getC_phone()%>"</td>
                             </tr>
                            <tr>
                                 <td class="text-capitalize active bg-light" colspan="10">2.Detail of company's person approving VSDC (Should be company owner or other person with power of attorney)</td>
                        
                             <tr>
                                 <td>Name</td>
                                 <td colspan="9"><input type="text" name="p_name" value="<%=vsdc.getPname()%>"</td>
                             </tr>
                                  </tr>
                                <tr>
                                 <td>Position</td>
                                 <td class="data" colspan="9"><input type="text" name="p_position" value="<%=vsdc.getPPosition()%>"</td>
                             </tr>
                              <tr>
                                 <td>Address</td>
                                 <td colspan="9"><input type="text" name="p_address" value="<%=vsdc.getPAddress()%>"</td>
                             </tr>
                              <tr>
                                 <td>Mobile Number</td>
                                 <td colspan="9"><input  required="" type="tel" name="person_phone" placeholder="07..."  minlength="10" maxlength="10" pattern="^\d{10}$" value="<%=vsdc.getPPhone()%>"/></td>
                             </tr>
                              <tr>
                                 <td>Email</td>
                                 <td colspan="9"><input type="email" name="person_email" required="" value="<%=vsdc.getpEmail()%>"/></td>
                             </tr>
                                    <tr>
                                 <td class="text-capitalize active bg-light" colspan="10">3.Device Information (For Supplier use only)</td>
                        
                             <tr>  
                               <tr class="">
                                 <td rowspan="2">
                                     <select name="device" value="<%=vsdc.getDevice()%>" id="device">
                                        <optgroup>
                                         <option id="laptop" value="laptop">LAPTOP</option>
                                         <option id="server" value="server">SERVER</option>
                                           <option id="desktop" value="desktop">DESKTOP</option>
                                            <option id="tablet" value="tablet">TABLET</option>
                                            
                                     </optgroup>  
                                     </select>
                                   
                                          </td>
                               
                                       <td colspan="9">
                                     Serial No.
                                    </td>
                                     </tr>
                                   <tr class="">
                                 <td colspan="9">
                                     <input type="text" name="serialnum" class="" value=" <%= vsdc.getSerialNum()%>"/>
                                    
                                 </td>
                          
                             </tr> 
                             <tr>
                                 <td>
                                     Location of Installation
                                 </td>
                                 <td colspan="9">KIGALI-RWANDA</td>
                             </tr>
                             <tr>
                                 <td >
                                     VSDC Supplier Tin
                                 </td>
                                 <td class="data">
                                     1
                                 </td>
                                  <td class="data">
                                     0
                                 </td>
                                  <td class="data">
                                     1
                                 </td>
                                  <td class="data">
                                     9
                                 </td>
                                  <td class="data">
                                     0
                                 </td>
                                 <td class="data">
                                     9
                                 </td>
                                  <td class="data">
                                     8
                                 </td>
                                  <td class="data">
                                     7
                                 </td>
                                  <td class="data">
                                     2
                                 </td>
                             </tr>
                             <tr>
                                 <td>
                                     VSCD Supplier Name
                                 </td> 
                                 <td colspan="9">
                                     ALGORITHM Inc. LTD
                                 </td>
                                 
                             </tr>
                             <tr>
                                 <td>
                                     VSCD Version
                                 </td>
                                 <td colspan="9">
                                     <%= vsdc.getVsdcVersion()%>
                                 </td>
                             </tr>
                                 <tr>
                                 <td>
                                     CIS Provider TIN & Name
                                 </td> 
                                 <td colspan="9">
                                  101909872   ALGORITHM Inc. LTD
                                 </td>
                                 
                             </tr>
                                 <tr>
                                 <td>
                                     CIS Name
                                 </td> 
                                 <td colspan="9">
                                     <%=vsdc.getCisName()%>
                                 </td>
                                 
                             </tr>
                                 <tr>
                                 <td>
                                     CIS Version 
                                 </td> 
                                 <td colspan="9">
                                     <%=vsdc.getCisVersion()%>
                                 </td>
                                 
                             </tr>
                                 <tr>
                                 <td>
                                     MRC Index
                                 </td> 
                                 <td colspan="9">
                                    <%=vsdc.getMrcIndex()%>
                                 </td>
                                 
                             </tr>
                         </table>
                     </div>
                     <div class="row mt-2">
                         <table>
                             <tr>
                                 <td colspan="2">
                                     <input type="checkbox" class="checkbox"  name="first_cond"/> On behalf of Recipient Company, I hereby confirm that I have received VSDC software in good condition. <br>
                                     <input type="checkbox" class="checkbox" name="second_cond"/> I understand that formatting this computer is not allowed, otherwise the action will be considered as fraudulent one.<br>
                                     <input type="checkbox" class="checkbox"  name ="third_cond"/> I will not proceed with formatting this computer until I get a written authorization from RRA upon an official submitted request.<br>
                                     <input name="forth_cond" type="checkbox" class="checkbox" /> The installed device must be a property of the taxpayer. <br>
                                     <input type="checkbox" class="checkbox" name="fith_cond"/> The company registration certificate must be attached to this form. <br>
                                     <input type="checkbox" class="checkbox"  name="sixth_cond"/> If approved person is the company's owner, attach the copy of ID. Otherwise, the person with power of attorney must have a recommendation from company's owner or managing director. <br>
                                     <input type="checkbox" class="checkbox"  name="seventh_cond"/> MRC: Keep the index VALG01 and replace the first three XXX by characters that represents the taxpayer and last XXX by number to represent the machine. Eg:VSDCALGO01KIP001
                                 </td>
                             </tr>
                             <tr>
                                 <td>
                                     Date ................../................./ <br>
                                     Signed on Behalf of Taxpayer .........<br>
                                     Name: ...........................<br>
                                     Tel: ..........................
                                 </td>
                                 <td>
                                        Date ................/............./ <br>
                                     Approved by VSDC Supplier: .........<br>
                                     Name: ...........................<br>
                                     Tel: .......................... 
                                 </td>
                             </tr>
                         </table>
                     </div>
                     </div>
                     <div id="print-btn"></div>
                     <%
                         if(vsdc.getStatus()==0){%>
                     <button disabled="true" class="btn-lg bg-success mt-2 ml-10" id="submit">Save your document</button>
                 <%} else{%>
                 
                   <button class="btn-lg bg-success mt-2 ml-10" type="" id="submit">Save your document</button>
                
                   <%}%>
                    </form>
                 </div>
                   
                            <%  }
                          
                            else{
                               %>
                                <form action="saveData" method="post">
                             <fieldset class="col-sm-12 col-md-5">
                                 <legend>  <label class="text-capitalize active bg-light">1.Company Information</label></legend>
                                 <div class="form-group" > <label for="company_name">Company Name: </label><input type="text" name="company_name" required=""/></div>
                                      
                                 <div class="form-group" > <label for="company_tin">TIN: </label><input type="number" name="company_tin" required=""/></div>
                                 <div class="form-group" > <label for="company_address">Address: </label><input type="text" name="company_address" required=""/></div>
                                 <div class="form-group" > <label for="company_email">Email: </label><input type="email" name="company_email" required=""/></div>
                                 <div class="form-group" > <label for="company_phone">Phone: </label><input type="tel" required="" name="company_phone" placeholder="07..."  minlength="10" maxlength="10" pattern="^\d{10}$"/></div>
                                                                  
                                                                         
                             </fieldset>
                                   <fieldset class="col-sm-12 col-md-5 ">
                                       <legend><label class="text-capitalize active bg-light">2.Detail of company's person approving VSDC (Should be company owner or other person with power of attorney)</label></legend>
                                 <div class="form-group" > <label for="person_name">Person Name: </label><input type="text" name="person_name" required=""/></div>
                                <div class="form-group" > <label for="person_position">Position: </label><input type="text" required="" name="person_position"/></div>
                                 <div class="form-group" > <label for="person_phone">Mobile Number: </label><input  required="" type="tel" name="person_phone" placeholder="07..."  minlength="10" maxlength="10" pattern="^\d{10}$"/></div>
                                                                  
                                 <div class="form-group" > <label for="person_address">Address: </label><input type="text" name="person_address" required=""/></div>
                                 <div class="form-group" > <label for="person_email">Email: </label><input type="email" name="person_email" required=""/></div>
                                                                                                          
                             </fieldset>
                             <fieldset class="col-sm-12 col-md-5">
                                 <legend><label class="text-capitalize active bg-light">3.Device Information (For supplier use only)</label></legend>
                                 <lable>Device:</lable> 
                                 <select class="select" name="device" required="">
                                     <optgroup>
                                         <option id="user_laptop" value="laptop">LAPTOP</option>
                                         <option id="user_server" value="server">SERVER</option>
                                           <option id="user_desktop" value="desktop">DESKTOP</option>
                                            <option id="user_tablet" value="tablet">TABLET</option>
                                            
                                     </optgroup>  
                                 </select>
                                 <br>
                                 <lable for="serialnum">Serial No:</lable><input type="text" name="serialnum" class=""/>
                             </fieldset>
                             <input class="form-control btn-success w-25 ml-md-4 m-2 btn-lg col-12" type="submit" value="Save">
                         </form>
                     </div>
                               <%
                             }
                         %>
                        
                    
            
    
                 <footer >
                     <div class="footer bg-primary align-items-center align-content-center">
                         
                     </div>
                 </footer>
     <script>
var doc = new jsPDF();
var specialElementHandlers = {
    '#print-btn': function (element, renderer) {
        return true;
    }
};

$('#submit').click(function () {
    
//saveDiv("section-to-print","myDoc");
  window.print(); 
//    doc.fromHTML($('#section-to-print').html(), 15, 15, {
//        'width': 170,
//            'elementHandlers': specialElementHandlers
//    });
//   doc.save('pdf-version.pdf');

});
 var doc = new jsPDF();

//Create PDf from HTML...
	function getPDF(){

		var HTML_Width = $(".section-to-print").width();
		var HTML_Height = $(".section-to-print").height();
		var top_left_margin = 15;
		var PDF_Width = HTML_Width+(top_left_margin*2);
		var PDF_Height = (PDF_Width*1.5)+(top_left_margin*2);
		var canvas_image_width = HTML_Width;
		var canvas_image_height = HTML_Height;
		
		var totalPDFPages = Math.ceil(HTML_Height/PDF_Height)-1;
		

		html2canvas($(".section-to-print")[0],{allowTaint:true}).then(function(canvas) {
			canvas.getContext('2d');
			
			console.log(canvas.height+"  "+canvas.width);
			
			
			var imgData = canvas.toDataURL("image/jpeg", 1.0);
			var pdf = new jsPDF('p', 'pt',  [PDF_Width, PDF_Height]);
		    pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin,canvas_image_width,canvas_image_height);
			
			
			for (var i = 1; i <= totalPDFPages; i++) { 
				pdf.addPage(PDF_Width, PDF_Height);
				pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height*i)+(top_left_margin*4),canvas_image_width,canvas_image_height);
			}
			
		    pdf.save("HTML-Document.pdf");
        });
	};
        </script>
 
     
   
    </body>
    
</html>
