/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import databaseDao.DocumentDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.*;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;
import databaseDao.UserDao;
import entities.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kwizera Claude
 */
@WebServlet(name = "UserController", urlPatterns = {"/"})
public class UserController extends HttpServlet {
    User user=null;
     DocumentDao documentdao;
    ArrayList<Document> documents=null;
private static final long serialVersionUID = 1L;
    private UserDao userDAO;
      @Override
    public void init() {
        userDAO = new UserDao();
         documentdao= new DocumentDao();
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String fname = request.getParameter("fname"); 
        String lname = request.getParameter("lname"); 
        String email = request.getParameter("email"); 
        String phoneNum=request.getParameter("phone");
        String gender=request.getParameter("gender");
        String password = request.getParameter("password");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UserController</title>");            
            out.println("</head>");
            out.println("<body>");
           out.println("<h4> you are "+fname+" "+lname+" with email: <a href="+'#'+">"+email+"</a></h4>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        String action = request.getServletPath();
         
        try {
              
            switch (action) {
                
                case "/register":
                    insertUser(request, response);
                    break;
                case "/delete":
                    deleteUser(request, response);
                    break;
                case "/edit":
                    showEditForm(request, response);
                    break;
                case "/update":
                    updateUser(request, response);
                    break;
                case "/logout":
                    logoutUser(request,response);
                    
                    break;
                      case "/login":{
                      loginUser(request,response);
                 
                      }
                    break;
                   
                     
                   
                    
            }
        } catch (SQLException ex) {
            throw new ServletException(ex);
        }
    

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       //processRequest(request, response);
       doGet(request,response);
   
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    private void listUser(HttpServletRequest request, HttpServletResponse response)
    throws SQLException, IOException, ServletException {
        List < User > listUser = userDAO.selectAllUsers();
    request.setAttribute("listUser", listUser);
        RequestDispatcher dispatcher = request.getRequestDispatcher("user-list.jsp");
        dispatcher.forward(request, response);
    }
 
private void loginUser(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
    System.out.println("Logging in................");
    String phoneNum=request.getParameter("lphone");
    String password = request.getParameter("lpassword");
     user= userDAO.login(phoneNum,password);
     if(user!=null){
         request.getSession().setAttribute("user", user);
          request.setAttribute("message","Successfully loggeg in!");
//          RequestDispatcher dispatcher = request.getRequestDispatcher("/");
//         dispatcher.forward(request, response);
      VsdcData vsdc=documentdao.selectVsdcByUserId(user.getId());
      if(vsdc!=null){
      System.out.println(user.toString());
      System.out.println(vsdc.toString());
      request.getSession().setAttribute("vsdc", vsdc);
      }
        response.sendRedirect(request.getContextPath()+"/");
     }
     else{
         System.out.println("User not found");
          request.setAttribute("message","User not found");
           RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
           dispatcher.forward(request, response);
        // response.sendRedirect("login.jsp");

     }
    
}
    private void showEditForm(HttpServletRequest request, HttpServletResponse response)
    throws SQLException, ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        User existingUser = userDAO.selectUser(id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("user-edit-form.jsp");
        request.setAttribute("user", existingUser);
        dispatcher.forward(request, response);

    }

    private void insertUser(HttpServletRequest request, HttpServletResponse response)
    throws SQLException, IOException, ServletException {
        System.out.println("Signup............");
        String fname = request.getParameter("fname"); 
        String lname = request.getParameter("lname"); 
        String email = request.getParameter("email"); 
        String phoneNum=request.getParameter("phone");
        String gender=request.getParameter("gender");
        String password = request.getParameter("password");
        //Long id, String fname, String lname, String email, String gender, String phoneNum, String password
        User newUser = new User(fname,lname, email,gender,phoneNum,password);
       boolean registered=userDAO.RegisterUser(newUser);
        //response.sendRedirect("list");
        if(registered){
            //processRequest(request,response);
            response.sendRedirect(request.getContextPath()+"/");
        }
        else{
        System.out.print("Unable to register user "+newUser.toString());
    }
    }

    private void updateUser(HttpServletRequest request, HttpServletResponse response)
    throws SQLException, IOException {
       Long id=Long.parseLong(request.getParameter("id"));
        String fname = request.getParameter("fname"); 
        String lname = request.getParameter("lname"); 
        String email = request.getParameter("email"); 
        String phoneNum=request.getParameter("phone");
        String gender=request.getParameter("gender");
        String password = request.getParameter("password");
      //first_name = ?,email= ?, last_name=?, telephone_number=? where id = ?
        User user = new User(id,fname,lname,phoneNum,email);
        userDAO.updateUser(user);
        response.sendRedirect("list");
    }

    private void deleteUser(HttpServletRequest request, HttpServletResponse response)
    throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        userDAO.deleteUser(id);
        response.sendRedirect("list");

    }
    private void saveUserData(HttpServletRequest request, HttpServletResponse response)
    throws SQLException, IOException {

    String c_tin_number,c_address,c_email,c_telephon,user_name,
     position,user_address,user_telephone,user_email,laptop,desktop,server,tablet,serialnumber,version;

}

    private void logoutUser(HttpServletRequest request, HttpServletResponse response) {
        try {
            request.getSession().invalidate();
            response.sendRedirect("index.jsp");
        } catch (IOException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}