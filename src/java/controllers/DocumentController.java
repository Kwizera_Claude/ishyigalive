/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import databaseDao.DocumentDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.*;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;
import databaseDao.UserDao;
import entities.*;
import jakarta.servlet.annotation.MultipartConfig;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Kwizera Claude
 */
@MultipartConfig
@WebServlet(name = "DocumentController", urlPatterns = {"/document"})
public class DocumentController extends HttpServlet {
    User user=null;
    VsdcData vsdc=null;
    DocumentDao documentdao;
   private static final long serialVersionUID = 1L;
   private final String UPLOAD_DIRECTORY = "D:\\Ishyiga works\\uploads/";
    ArrayList<Document> documents=null;
        public void init() {
        documentdao= new DocumentDao();
       
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String company=request.getParameter("company");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DocumentController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DocumentController at " + request.getContextPath() + "<br> your company is"+company+"</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         
          request.getSession().setAttribute("documents", documents);
         String action = request.getServletPath();
        
        try {
              
            switch (action) {
                
                case "/saveData":
                    insertDocument(request, response);
                    break;
                case "/delete":
                    deleteDocument(request, response);
                    break;
                
                case "/update":
                    updatedocument(request, response);
                    
                    break;
                    case "/upload":
                    uploaddocument(request, response);
                    
                    break;
                    case "/documents":
                        RequestDispatcher dispatcher=request.getRequestDispatcher("documents.jsp");
                        dispatcher.include(request, response);
                    //response.sendRedirect("documents.jsp");
                    break;
                    case "/download":
                        downloadDocument(request,response);
                        break;
                    
                      
                          
            }
        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       doGet(request,response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void insertDocument(HttpServletRequest request, HttpServletResponse response) throws IOException {
            response.setContentType("text/html;charset=UTF-8");
 String company;int tin;String c_address;String c_email;String c_phone;String pname;String PPosition;String PAddress;String PPhone;String pEmail;String device;String serialNum;int userId;
 
 company=request.getParameter("company_name");
 System.out.println("company: "+company);
 tin=Integer.parseInt(request.getParameter("company_tin"));
 c_address=request.getParameter("company_address");
  c_email=request.getParameter("company_email");
  c_phone=request.getParameter("company_phone");
    pname=request.getParameter("person_name");
     PPosition=request.getParameter("person_position");
      PAddress=request.getParameter("person_address");
       PPhone=request.getParameter("person_phone");  
       pEmail=request.getParameter("person_email");
       device=request.getParameter("device");
       serialNum=request.getParameter("serialnum");
       userId=0;
       user=(User)request.getSession().getAttribute("user");
       if(user!=null){
           System.out.println(user.toString());
       userId=(int)(long)user.getId();
       System.out.println("User id: "+userId);
         vsdc=new VsdcData();
       vsdc.setUserId(userId);
        vsdc.setCompany(company);
            vsdc.setC_address(c_address);
            vsdc.setC_address(c_address);
           vsdc.setC_email(c_email);
            vsdc.setC_phone(c_phone);
        
            vsdc.setDevice(device);
            vsdc.setPAddress(PAddress);
            vsdc.setPAddress(PAddress);
            vsdc.setSerialNum(serialNum);
            vsdc.setSerialNum(serialNum);
            vsdc.setStatus(0);
            vsdc.setTin(tin);
            vsdc.setPPhone(PPhone);
            vsdc.setpEmail(pEmail);
            vsdc.setPname(pname);
            vsdc.setPPosition(PPosition);
            
               
            System.out.println(vsdc.toString());
                 boolean isSent=documentdao.InsertRevenueData(vsdc);
            if(isSent){
                 request.getSession().setAttribute("vsdc",vsdc);
                 response.sendRedirect(request.getContextPath()+"/");
            }
            else{
                System.out.println("Data not sent!!");
            }
       }
    
    }

    private void deleteDocument(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    private void updatedocument(HttpServletRequest request, HttpServletResponse response) throws IOException {
        System.out.println(request.getSession().toString());
        try {
            //processRequest(request,response);
          String company;int tin;String c_address;String c_email;String c_phone;String pname;String PPosition;String PAddress;String PPhone;String pEmail;String device;String serialNum;int userId;
         tin=Integer.parseInt(request.getParameter("tin"));
         company=request.getParameter("company");
         c_address=request.getParameter("c_address");
        c_email=request.getParameter("c_email");
        c_phone=request.getParameter("c_phone");
        pname=request.getParameter("p_name");
        PPosition=request.getParameter("p_position");
        PAddress=request.getParameter("p_address");
        PPhone=request.getParameter("person_phone");  
        pEmail=request.getParameter("person_email");
        device=request.getParameter("device");
        serialNum=request.getParameter("serialnum");
        userId=0;
         User user=(User)request.getSession().getAttribute("user");
       if(user!=null){
           System.out.println(user.toString());
       userId=(int)(long)user.getId();
       System.out.println("User id: "+userId);
             VsdcData vsdc=(VsdcData)request.getSession().getAttribute("vsdc");
             vsdc.setUserId(userId);
             vsdc.setCompany(company);
            vsdc.setC_address(c_address);
            vsdc.setC_address(c_address);
           vsdc.setC_email(c_email);
            vsdc.setC_phone(c_phone);
        
            vsdc.setDevice(device);
            vsdc.setPAddress(PAddress);
            vsdc.setPAddress(PAddress);
            vsdc.setSerialNum(serialNum);
            vsdc.setSerialNum(serialNum);
            vsdc.setStatus(0);
            vsdc.setTin(tin);
            vsdc.setPPhone(PPhone);
            vsdc.setpEmail(pEmail);
            vsdc.setPname(pname);
            vsdc.setPPosition(PPosition);
           
           boolean updated=documentdao.updateVsdcData(vsdc);
           if(updated){
               System.out.println("Well updated to=> "+vsdc.toString());
           }
           else{
               System.out.println("Unable to update!");
           }
           response.sendRedirect(request.getContextPath()+"/");
       }
            //response.sendRedirect(request.getContextPath()+"/");
        } catch (Exception ex) {
            Logger.getLogger(DocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void uploaddocument(HttpServletRequest request, HttpServletResponse response) {
        ArrayList<String> Urls=new ArrayList();
   try {
    final Collection<Part> parts = request.getParts();
    for (final Part part : parts) {
        String url=UPLOAD_DIRECTORY+part.getSubmittedFileName();
       part.write(url);
       Urls.add(url);
     
       
    }
      user=(User)request.getSession().getAttribute("user");
      Document doc=new Document("FirstDoc",Urls.get(0),user);
    boolean isUploaded= documentdao.InsertDocument(doc);
    System.out.println("Uploaded? "+isUploaded);
    ArrayList<Document> documents=new ArrayList();
    documents.add(doc);
     response.sendRedirect(request.getContextPath()+"/documents");

    } catch (Exception e) {
      
}
    }
    public  List<Document> findDocuments(User user){
            List<Document> documents=new ArrayList();
                  documentdao= new DocumentDao();
            documents=documentdao.listDocumentByUserId(user.getId());
            System.out.println("my docs:"+documents);
            return documents;
    }

    private void downloadDocument(HttpServletRequest request, HttpServletResponse response) throws FileNotFoundException, IOException {
        FileInputStream fis = null;
           String url = request.getRequestURL().toString();
            String fileLocation = request.getParameter("url"); //change the path according to you
            System.out.println("URL:  "+fileLocation);
            File file = new File(fileLocation);
            fis = new FileInputStream(file);
            ServletOutputStream sos;
                sos = response.getOutputStream();
            
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=" + fileLocation);
            byte[] buffer = new byte[4096];
            try {
                while((fis.read(buffer, 0, 4096)) != -1){
                    sos.write(buffer, 0, 4096);
                }
            } catch (IOException ex) {
                Logger.getLogger(DocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
         fis.close();
        }
}
        


