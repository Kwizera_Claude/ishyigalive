/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databaseDao;

import static databaseDao.DbConfig.getConnection;
import entities.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kwizera Claude
 */
public class UserDao {
    
    private static final String INSERT_USERS_SQL = "INSERT INTO users(first_name,last_name,email,telephone_number,gender,password,salt,Role)VALUES(?,?,?,?,?,?,?,?)";
    private static final String SELECT_USER_BY_ID = "select * from users where id =?";
    private static final String SELECT_ALL_USERS = "select * from users";
    private static final String DELETE_USERS_SQL = "delete from users where id =?;";
    private static final String UPDATE_USERS_SQL = "update users set first_name = ?,email= ?, last_name=?, telephone_number=? where id = ?;";
      PreparedStatement preparedStatement;
    public User selectUser(long id) {
        User user = null;
        // Step 1: Establishing a Connection
        try (Connection connection = getConnection();
            // Step 2:Create a statement using connection object
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_BY_ID);) {
            preparedStatement.setLong(1, id);
            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();

            // Step 4: Process the ResultSet object.
            while (rs.next()) {
                String fname = rs.getString("first_name");
                String email = rs.getString("email");
                String lname = rs.getString("last_name");
                String gender=rs.getString("gender");
                String phoneNum=rs.getString("telephone_number");
                user = new User(id, fname,lname,email, gender, phoneNum);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return user;
    }
private List<User> findUsers( ResultSet rs ){
      User user = null;
        List < User > users = new ArrayList < > ();
        try {
            while (rs.next()) {
                long id = rs.getLong("id");
                String fname = rs.getString("first_name");
                String email = rs.getString("email");
                String lname = rs.getString("last_name");
                String gender=rs.getString("gender");
                String phoneNum=rs.getString("telephone_number");
                user=new User(id, fname,lname,email, gender, phoneNum);
               users.add(user);
            }       } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
         return users;
}
    public List<User> selectAllUsers() throws SQLException {

        // using try-with-resources to avoid closing resources (boiler plate code)
        List <User> users = new ArrayList <> ();
        // Step 1: Establishing a Connection
        try (Connection connection = getConnection();

            // Step 2:Create a statement using connection object
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_USERS);) {
            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();
            users= findUsers(rs );

        return users;
        }
    }
    
    public List<User> listByName() throws SQLException {
        List<User> listUsers = new ArrayList<>();
         
        try  {
            String sql = "SELECT *FROM users ORDER BY name";
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(sql);
            listUsers=findUsers(result);        
             
        } catch (SQLException ex) {
            throw ex;
        }      
         
        return listUsers;
    }

    public boolean deleteUser(long id) throws SQLException {
        boolean rowDeleted;
        try (Connection connection = getConnection(); 
             PreparedStatement statement = connection.prepareStatement(DELETE_USERS_SQL);) {
            statement.setLong(1, id);
            rowDeleted = statement.executeUpdate() > 0;
        }
        return rowDeleted;
    }

    public boolean updateUser(User user) throws SQLException {
        boolean rowUpdated;
        try (Connection connection = getConnection(); 
                //first_name = ?,email= ?, last_name=?, telephone_number
            PreparedStatement statement = connection.prepareStatement(UPDATE_USERS_SQL);) {
            statement.setString(1, user.getFname());
            statement.setString(2, user.getEmail());
            statement.setString(3, user.getLname());
            statement.setString(4,user.getPhoneNum());
            statement.setLong(5, user.getId());
            rowUpdated = statement.executeUpdate() > 0;
        }
        return rowUpdated;
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
     public boolean RegisterUser(User user) {
         boolean isRegistered=false;
     System.out.println("The user"+user.toString());
        try {
       String fn,ln,email,phone,gender,pass;
       fn=user.getFname();
       ln=user.getLname();
       email=user.getEmail();
       phone=user.getPhoneNum();
       gender=user.getGender();
       pass=user.getPassword();
            String salt = PasswordUtils.getSalt(30);
            String securedpass = PasswordUtils.generateSecurePassword(pass, salt);
       
            if (!email.isEmpty()) {
           
              Connection connection = getConnection();
                preparedStatement = connection.prepareStatement("select *from users where email=?");
                preparedStatement.setString(1, email);
               ResultSet Result = preparedStatement.executeQuery();
//first_name,last_name,email,telephone_number,gender,password,salt
                if (Result.next()) {
                    System.out.println("Email"+Result.getString("email"));
                    System.out.println("The user exists!");
                } else {
                    preparedStatement = connection.prepareStatement(INSERT_USERS_SQL);
                    preparedStatement.setString(1, fn);
                    preparedStatement.setString(2, ln);
                    preparedStatement.setString(3, email);
                    preparedStatement.setString(4, phone);
                      preparedStatement.setString(5,gender);
                    preparedStatement.setString(6, securedpass);
                    preparedStatement.setString(7, salt);
                    preparedStatement.setInt(8, 0);      
                    preparedStatement.executeUpdate();
                  
                    System.out.println("Well Registered with  email of :" + email + "!");
                 isRegistered=true;
              
                }
            } else {
             System.out.println( " Email must be filled!");
            }

        } catch (SQLException e) {
           System.out.println("Error:  "+e.getMessage());
        }
        return isRegistered;
    }

    public User login(String phoneNum, String password) throws SQLException {
        System.out.println(phoneNum);
             User user = null;
             
        Connection connection = getConnection();
                String securedpass = "";
                boolean truelyuser = false;
                String salt = "";
                  String querry = "select *from users where telephone_number=?";
              
            preparedStatement = connection.prepareStatement(querry);
            preparedStatement.setString(1, phoneNum);
            ResultSet usercredentials = preparedStatement.executeQuery();
            if (usercredentials.next()) {
                securedpass = usercredentials.getString("Password");
                salt = usercredentials.getString("salt");
//                usertype = usercredentials.getString("user_type");
            
            truelyuser = PasswordUtils.verifyUserPassword(password, securedpass, salt);
            if(truelyuser){
            System.out.println("is user found? " + truelyuser);
            user=new User();
            user.setId(usercredentials.getLong("id"));
            user.setEmail(usercredentials.getString("email"));
            user.setFname(usercredentials.getString("first_name"));
            user.setLname(usercredentials.getString("last_name"));
            user.setGender(usercredentials.getString("gender"));
            user.setFname(usercredentials.getString("first_name"));
            user.setPhoneNum(phoneNum);
            user.setRole(usercredentials.getInt("Role"));
            }
            }else{
                user=null;
            }
//first_name,last_name,email,telephone_number,gender,password,salt

       
        return user;
    }
}

