/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databaseDao;

import static databaseDao.DbConfig.getConnection;
import entities.Document;
import entities.User;
import entities.VsdcData;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kwizera Claude
 */
public class DocumentDao {
     PreparedStatement preparedStatement;
     private static final String UPDATE_VSDC="UPDATE `vsdcdata` SET `company`=?,`Tin`=?,`c_address`=?,`c_email`=?,`c_phone`=?,`PAddress`=?,`Pname`=?,`Pposition`=?,`Pphone`=?,`Pemail`=?,`device`=?,`serialnumber`=?,`status`=?,`vsdcVersion`=?,`cisName`=?,`cisVersion`=?,`mrcIndex`=? WHERE id=?";
     private static final String selectvsdcByUserId="select * from vsdcdata where userId=?";
     private static final String SELECT_USER_BY_ID = "select * from users where id =?";
     private static final String INSERT_DOCUMENT_SQL = "INSERT INTO `documents`(`userId`, `document_name`, `url`) VALUES (?,?,?)";
    private static final String SELECT_DOCUMENT_BY_ID = "select * from documents where id =?";
    private static final String SELECT_DOCUMENT_BY_NAME = "select * from documents where document_name =?";
    private static final String SELECT_ALL_DOCUMENTS = "select * from documents";
    private static final String DELETE_DOCUMENT_SQL = "delete from ducuments where id =?;";
    private static final String UPDATE_DOCUMENT_SQL = "UPDATE `documents` SET ``document_name`=?,`url`=? WHERE document_name=?";
public User selectUser(long id) {
        User user = null;
        // Step 1: Establishing a Connection
        try (Connection connection = getConnection();
            // Step 2:Create a statement using connection object
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_BY_ID);) {
            preparedStatement.setLong(1, id);
            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();

            // Step 4: Process the ResultSet object.
            while (rs.next()) {
                String fname = rs.getString("first_name");
                String email = rs.getString("email");
                String lname = rs.getString("last_name");
                String gender=rs.getString("gender");
                String phoneNum=rs.getString("telephone_number");
                user = new User(id, fname,lname,email, gender, phoneNum);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return user;
    }
public Document selectDocument(long id) {
        Document document = null;
        // Step 1: Establishing a Connection
        try (Connection connection = getConnection();
            // Step 2:Create a statement using connection object
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_DOCUMENT_BY_ID);) {
            preparedStatement.setLong(1, id);
            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();

            // Step 4: Process the ResultSet object.
            while (rs.next()) {
                String docName = rs.getString("document_name");
                String url = rs.getString("url");
                int userId = rs.getInt("userId");
              
                User user=selectUser(userId);
               document=new Document();
               document.setId(id);
               document.setName(docName);
               document.setUrl(url);
               document.setUser(user);
            
               
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return document;
    }
public VsdcData selectVsdcByUserId(long userId){
    VsdcData vsdc=null;
         try {
             
             Connection connection = getConnection();
             // Step 2:Create a statement using connection object
             preparedStatement=connection.prepareStatement(selectvsdcByUserId);
             preparedStatement.setLong(1,userId);
              System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();
//SELECT `id`, `company`, `Tin`, `c_address`, `c_email`, `c_phone`, `PAddress`, `Pname`, `Pposition`, `Pphone`, `Pemail`, `device`, `serialnumber`, `status`, `vsdcVersion`, `cisName`, `cisVersion`, `mrcIndex`, `userId` FROM `vsdcdata` WHERE 1
            // Step 4: Process the ResultSet object.
            while (rs.next()) {
                   String company;int tin;String c_address;String c_email;String c_phone;String pname;String PPosition;String PAddress;String PPhone;String pEmail;String device;String serialNum;
                   String vsdcVersion, cisName,cisVersion,mrcIndex; 
                   int status;
          vsdc=new VsdcData();
          vsdc.setCompany(rs.getString("company"));
          vsdc.setC_address(rs.getString("c_address"));
          vsdc.setC_email(rs.getString("c_email"));
          vsdc.setC_phone(rs.getString("c_phone"));
          vsdc.setCisName(rs.getString("cisName"));
          vsdc.setCisVersion(rs.getString("cisVersion"));
          vsdc.setTin(rs.getInt("Tin"));
          vsdc.setDevice(rs.getString("device"));
          vsdc.setMrcIndex(rs.getString("mrcIndex"));
          vsdc.setPAddress(rs.getString("PAddress"));
          vsdc.setpEmail(rs.getString("Pemail"));
          vsdc.setPname(rs.getString("Pname"));
          vsdc.setPPosition(rs.getString("Pposition"));
          vsdc.setPPhone(rs.getString("Pphone"));
          vsdc.setSerialNum(rs.getString("serialnumber"));
          vsdc.setStatus(Integer.parseInt(rs.getString("status")));
          vsdc.setUserId((int)(long)userId);
          vsdc.setId(rs.getInt("id"));
          
            }
                
              
         } catch (SQLException ex) {
             Logger.getLogger(DocumentDao.class.getName()).log(Level.SEVERE, null, ex);
         }
       
 
           return vsdc;
}
public Document selectDocumentByName(String name) {
        Document document = null;
        // Step 1: Establishing a Connection
        try (Connection connection = getConnection();
            // Step 2:Create a statement using connection object
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_DOCUMENT_BY_NAME);) {
            preparedStatement.setString(1, name);
            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();

            // Step 4: Process the ResultSet object.
            while (rs.next()) {
                int id=rs.getInt("id");
                String docName = rs.getString("document_name");
                String url = rs.getString("url");
                int userId = rs.getInt("userId");
                
                User user=selectUser(userId);
               document=new Document();
               document.setId(id);
               document.setName(docName);
               document.setUrl(url);
               document.setUser(user);
               
               
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return document;
    }
private List<Document> findDocuments( ResultSet rs ){
      Document document= null;
        List <Document > documents = new ArrayList < > ();
        try {
            while (rs.next()) {
                long id = rs.getLong("id");
                String docName = rs.getString("document_name");
                String url = rs.getString("url");
                int userId = rs.getInt("userId");
                
                User user=selectUser(userId);
               document=new Document();
               document.setId(id);
               document.setName(docName);
               document.setUrl(url);
               document.setUser(user);
              System.out.println("Found: "+document.toString());
               documents.add(document);
            }       } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
         return documents;
}
    public List<Document> selectAllDocuments() throws SQLException {

        // using try-with-resources to avoid closing resources (boiler plate code)
        List <Document> documents = new ArrayList <> ();
        // Step 1: Establishing a Connection
        try (Connection connection = getConnection();

            // Step 2:Create a statement using connection object
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_DOCUMENTS);) {
            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();
            documents= findDocuments(rs );

        return documents;
        }
    }
public List<Document> listDocumentByUserId(long userId){
    List<Document> listDocuments = new ArrayList<>();
    String query="select * from documents where userId=?";
     Connection connection = getConnection();
         try {
             PreparedStatement preparedStatement = connection.prepareStatement(query);
             preparedStatement.setLong(1, userId);
             ResultSet result=preparedStatement.executeQuery();
             listDocuments=findDocuments(result);
             System.out.println("Documents: "+listDocuments.toString());
         } catch (SQLException ex) {
             Logger.getLogger(DocumentDao.class.getName()).log(Level.SEVERE, null, ex);
         }
    return listDocuments;
}    
    public List<Document> listByName() throws SQLException {
        List<Document> listDocuments = new ArrayList<>();
         
        try  {
            String sql = "SELECT *FROM documents ORDER BY document_name";
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(sql);
            listDocuments=findDocuments(result);        
             
        } catch (SQLException ex) {
            throw ex;
        }      
         
        return listDocuments;
    }

    public boolean deleteDocument(long id) throws SQLException {
        boolean rowDeleted;
        try (Connection connection = getConnection(); 
             PreparedStatement statement = connection.prepareStatement(DELETE_DOCUMENT_SQL);) {
            statement.setLong(1, id);
            rowDeleted = statement.executeUpdate() > 0;
        }
        return rowDeleted;
    }

    public boolean updateDocument(Document document) throws SQLException {
        boolean rowUpdated;
        try (Connection connection = getConnection(); 
                //first_name = ?,email= ?, last_name=?, telephone_number
            PreparedStatement statement = connection.prepareStatement(UPDATE_DOCUMENT_SQL);) {
            statement.setString(1, document.getName());
            statement.setString(2, document.getUrl());
         
          
            rowUpdated = statement.executeUpdate() > 0;
        }
        return rowUpdated;
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
     public boolean InsertDocument(Document document) {
         boolean isSent=false;
     System.out.println("The document"+document.toString());
        try {
       String docName,url;
       long userId;
       int status;
            docName=document.getName();
            url=document.getUrl();
            userId=document.getUser().getId();
           
           
            if (!docName.isEmpty()) {
            PreparedStatement preparedStatement;
              Connection connection = getConnection();
       
                    preparedStatement = connection.prepareStatement(INSERT_DOCUMENT_SQL);
                    preparedStatement.setLong(1,userId);
                    preparedStatement.setString(2, docName);
                    preparedStatement.setString(3, url);
            
                   
                    preparedStatement.executeUpdate();
                  
                  System.out.println("Well submitted the document:" + docName + "!");
                 isSent=true;
              
                
            } else {
             System.out.println( " Document is required !");
            }

        } catch (SQLException e) {
           System.out.println("Error:  "+e.getMessage());
        }
        return isSent;
    }
     public boolean InsertRevenueData(VsdcData vsdc) {
       
          boolean isRegistered=false;
          Connection connection = getConnection();
       String INSERT_REVENUE_DATA="insert into vsdcdata(company,Tin,c_address,c_email,c_phone,Pname,Pposition,Pphone,Pemail,PAddress,device,serialnumber,status,userId) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            try { 
                preparedStatement = connection.prepareStatement(INSERT_REVENUE_DATA);
                preparedStatement.setString(1,vsdc.getCompany());
        preparedStatement.setInt(2,vsdc.getTin());
        preparedStatement.setString(3,vsdc.getC_address());
        preparedStatement.setString(4,vsdc.getC_email());
        preparedStatement.setString(5, vsdc.getC_phone());
        preparedStatement.setString(6,vsdc.getPname());
        preparedStatement.setString(7,vsdc.getPPosition());
        preparedStatement.setString(8,vsdc.getPPhone());
        preparedStatement.setString(9,vsdc.getpEmail());
        preparedStatement.setString(10,vsdc.getPAddress());
        preparedStatement.setString(11,vsdc.getDevice());
        preparedStatement.setString(12,vsdc.getSerialNum());
        preparedStatement.setInt(13,vsdc.getStatus());
       preparedStatement.setInt(14,vsdc.getUserId());
       preparedStatement.executeLargeUpdate();
       System.out.println("Well sent: ");
        isRegistered=true;
            } catch (SQLException ex) {
                System.out.println("Document exception: "+ex.getMessage());
            }
        return isRegistered;
     }
public boolean updateVsdcData(VsdcData vsdc){
    boolean isUpdated=false;
     Connection connection = getConnection();
    
    try{
        preparedStatement = connection.prepareStatement(UPDATE_VSDC);
        preparedStatement.setString(1,vsdc.getCompany());
        preparedStatement.setInt(2,vsdc.getTin());
        preparedStatement.setString(3,vsdc.getC_address());
        preparedStatement.setString(4,vsdc.getC_email());
        preparedStatement.setString(5, vsdc.getC_phone());
        preparedStatement.setString(6,vsdc.getPAddress());
        preparedStatement.setString(7,vsdc.getPname());
        preparedStatement.setString(8,vsdc.getPPosition());
        preparedStatement.setString(9,vsdc.getPPhone());
        preparedStatement.setString(10,vsdc.getpEmail());
        preparedStatement.setString(11,vsdc.getDevice());
        preparedStatement.setString(12,vsdc.getSerialNum());
        preparedStatement.setInt(13,vsdc.getStatus());
        preparedStatement.setString(14,vsdc.getVsdcVersion());
        preparedStatement.setString(15,vsdc.getCisName());
        preparedStatement.setString(16,vsdc.getCisVersion());
         preparedStatement.setString(17,vsdc.getMrcIndex());
         preparedStatement.setInt(18,vsdc.getId());
         if(vsdc.getVsdcVersion()!=null && vsdc.getCisName()!=null && vsdc.getCisVersion()!=null &&vsdc.getMrcIndex()!=null ){
             vsdc.setStatus(1);
              preparedStatement.setInt(13,1);
         }
        preparedStatement.executeLargeUpdate();
       System.out.println("Well sent: ");
        isUpdated=true;
    }
    catch (SQLException e) {
           System.out.println("Error:  "+e.getMessage());
        }
    return isUpdated;
}
   
}

