/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databaseDao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kwizera Claude
 */
public class DbConfig {
private static final String jdbcURL = "jdbc:mysql://localhost/ishyigalive?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
private static final String jdbcUsername = "root";
private static final String jdbcPassword = "";
     static Connection getConnection() {
      Connection connection = null;
        try {
    try {
        Class.forName("com.mysql.jdbc.Driver");   //register driver
    } catch (ClassNotFoundException ex) {
        Logger.getLogger(DbConfig.class.getName()).log(Level.SEVERE, null, ex);
    }

      connection=DriverManager.getConnection(jdbcURL ,jdbcUsername ,jdbcPassword);
      System.out.println("Connected successfully!");
        } catch (SQLException e) {
            // TODO Auto-generated catch block
 System.out.println("Failed to connect! due to "+e.getMessage());
 
        }
    // TODO Auto-generated catch block
        return connection;
    }
     public static void main(String[] args){
         getConnection(); 
     }
}
