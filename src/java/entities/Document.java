/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author Kwizera Claude
 */
public class Document {
 private String name;
 private String url;
 private User user;
 private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Document() {
        
    }

    @Override
    public String toString() {
        return "Document{" + "name=" + name + ", url=" + url +  ", user=" + user + '}';
    }
 

    public Document(String name, String url, User user) {
        this.name = name;
        this.url = url;
        this.user = user;
    }

    public Document(String name, User user) {
        this.name = name;
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
 
}
