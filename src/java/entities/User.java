/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;


/**
 *
 * @author Kwizera Claude
 */

public class User implements Serializable {

   public User(Long id, String fname, String lname, String phone, String email) {
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.email = email;
     
    }
    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUploadedfile() {
        return uploadedfile;
    }

    public void setUploadedfile(String uploadedfile) {
        this.uploadedfile = uploadedfile;
    }

    public User() {
    }

    private static final long serialVersionUID = 1L;
    
    private Long id;

    private String fname;

    private String lname;

    private String email;
    private String gender;
    private String uploadedfile;
    private String phoneNum;
    private String password;
    private String salt;
    private int role;

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public User(String fname, String lname, String email, String gender, String phoneNum, String password) {
      
        this.fname = fname;
        this.lname = lname;
        this.email = email;
        this.gender = gender;
        this.phoneNum = phoneNum;
        this.password = password;
    }
    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public User(Long id, String fname, String lname, String email, String gender, String phoneNum) {
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.email = email;
        this.gender = gender;
        this.phoneNum = phoneNum;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof User)) {
            return false;
        }
        User other = (User) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", fname=" + fname + ", lname=" + lname + ", email=" + email + ", gender=" + gender + ", uploadedfile=" + uploadedfile + ", phoneNum=" + phoneNum + '}';
    }

    
 
    
}
