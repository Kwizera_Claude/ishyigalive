/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author Kwizera Claude
 */
public class VsdcData {
     String company;int tin;String c_address;String c_email;String c_phone;String pname;String PPosition;String PAddress;String PPhone;String pEmail;String device;String serialNum;int userId;
     String vsdcVersion, cisName,cisVersion,mrcIndex; 
     int status;
     int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "VsdcData{" + "company=" + company + ", tin=" + tin + ", c_address=" + c_address + ", c_email=" + c_email + ", c_phone=" + c_phone + ", pname=" + pname + ", PPosition=" + PPosition + ", PAddress=" + PAddress + ", PPhone=" + PPhone + ", pEmail=" + pEmail + ", device=" + device + ", serialNum=" + serialNum + ", userId=" + userId + ", vsdcVersion=" + vsdcVersion + ", cisName=" + cisName + ", cisVersion=" + cisVersion + ", mrcIndex=" + mrcIndex + ", status=" + status + ", id=" + id + '}';
    }
 

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public int getTin() {
        return tin;
    }

    public void setTin(int tin) {
        this.tin = tin;
    }

    public String getC_address() {
        return c_address;
    }

    public void setC_address(String c_address) {
        this.c_address = c_address;
    }

    public String getC_email() {
        return c_email;
    }

    public void setC_email(String c_email) {
        this.c_email = c_email;
    }

    public String getC_phone() {
        return c_phone;
    }

    public void setC_phone(String c_phone) {
        this.c_phone = c_phone;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getPPosition() {
        return PPosition;
    }

    public void setPPosition(String PPosition) {
        this.PPosition = PPosition;
    }

    public String getPAddress() {
        return PAddress;
    }

    public void setPAddress(String PAddress) {
        this.PAddress = PAddress;
    }

    public String getPPhone() {
        return PPhone;
    }

    public void setPPhone(String PPhone) {
        this.PPhone = PPhone;
    }

    public String getpEmail() {
        return pEmail;
    }

    public void setpEmail(String pEmail) {
        this.pEmail = pEmail;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getSerialNum() {
        return serialNum;
    }

    public void setSerialNum(String serialNum) {
        this.serialNum = serialNum;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

   

    public String getVsdcVersion() {
        return vsdcVersion;
    }

    public void setVsdcVersion(String vsdcVersion) {
        this.vsdcVersion = vsdcVersion;
    }

    public String getCisName() {
        return cisName;
    }

    public void setCisName(String cisName) {
        this.cisName = cisName;
    }

    public String getCisVersion() {
        return cisVersion;
    }

    public void setCisVersion(String cisVersion) {
        this.cisVersion = cisVersion;
    }

    public String getMrcIndex() {
        return mrcIndex;
    }

    public void setMrcIndex(String mrcIndex) {
        this.mrcIndex = mrcIndex;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public VsdcData() {
    }

    public VsdcData(String company, int tin, String c_address, String c_email, String c_phone, String vsdcVersion, String cisName, String cisVersion, String mrcIndex, int status) {
        this.company = company;
        this.tin = tin;
        this.c_address = c_address;
        this.c_email = c_email;
        this.c_phone = c_phone;
        this.pname = pname;
        this.PPosition = PPosition;
        this.PAddress = PAddress;
        this.PPhone = PPhone;
        this.pEmail = pEmail;
        this.device = device;
        this.serialNum = serialNum;
        this.userId = userId;
       
        this.vsdcVersion = vsdcVersion;
        this.cisName = cisName;
        this.cisVersion = cisVersion;
        this.mrcIndex = mrcIndex;
        this.status = status;
    }

}